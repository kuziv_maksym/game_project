package actions;

import characters.MainPersone;
import characters.Monster;

import java.io.FileWriter;
import java.io.IOException;

public class SystemExit {
    static FileWriter fw;

    static {
        try {
            fw = new FileWriter("Saves/Save.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public SystemExit() throws IOException {
    }

    public static void systemExit(MainPersone hero, Monster monster){
        try {
            fw.write(hero.getMaxHp() + " " + hero.getHp() + " " +
                    hero.getAgi() + " " + hero.getBra() + " " + hero.getPow()
                    + " " + hero.getName() + " ");
            fw.write(monster.getHp() + " " + monster.getPow() +
                    " " + monster.getName());
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.exit(0);
    }
}
