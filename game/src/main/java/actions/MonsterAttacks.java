package actions;

import characters.MainPersone;
import characters.Monster;

public class MonsterAttacks {
    public static void monsterAttacks(MainPersone hero, Monster monster){
        hero.setHp(hero.getHp() - monster.getPow());
        if (hero.getHp() >= 0) {
            System.out.println("Монстр ударив Вас і у Вас залишилось " + hero.getHp() + " hp");
        } else {
            System.out.println("Ви вмерли");
        }
    }
}
