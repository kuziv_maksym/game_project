package actions;

import java.util.Scanner;
import characters.MainPersone;
import characters.Monster;
import mvc.View;

import static actions.Evade.evade;
import static actions.Healing.healing;
import static actions.HeroAttacks.heroAttacks;
import static actions.MonsterAttacks.monsterAttacks;
import static actions.SystemExit.systemExit;

public class Fight {
    public static void fight(Monster monster, MainPersone hero) {
        Scanner sc = new Scanner(System.in);
        while (monster.getHp() > 0 && hero.getHp() > 0) {
            View.display("Виберіть вашу дію:");
            View.display("1. Атакувати");
            View.display("2. Лікуватись");
            View.display("3. Вихід");
            int x = sc.nextInt();
            if (x == 1) {
                heroAttacks(hero, monster);
                if (monster.getHp() > 0) {
                    if (evade(hero)) {
                        monsterAttacks(hero, monster);
                    }
                }
            } else if (x == 2) {
                healing(hero);
                if (monster.getHp() > 0) {
                    if (evade(hero)) {
                        monsterAttacks(hero, monster);
                    }
                }
            } else if (x == 3){
                systemExit(hero, monster);
            }
        }
    }
}
