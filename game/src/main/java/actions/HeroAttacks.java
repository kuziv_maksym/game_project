package actions;

import characters.MainPersone;
import characters.Monster;
import characters.Persone;
import mvc.View;

public class HeroAttacks {
    public static void heroAttacks(MainPersone hero, Monster monster){
        monster.setHp(monster.getHp() - hero.getPow());

        if (monster.getHp() >=0 ) {
            View.display("Ви вдарили монстра і в нього залишилось " + monster.getHp() + " hp");
        } else {
            View.display("Монстер умер");
        }
    }
}
