package actions;

import characters.MainPersone;

import java.util.Random;

public class Evade {
    public static boolean evade(MainPersone hero){
        int min = 0;
        int max = 100;
        int diff = max - min;
        Random random = new Random();
        int y = random.nextInt(diff + 1);
        y += min;
        if (y < hero.getAgi()/5) {
            System.out.println("Ви вклонились");
            return false;
        } else {
            return true;
        }
    }
}
