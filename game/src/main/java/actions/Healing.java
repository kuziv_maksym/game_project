package actions;

import characters.MainPersone;
import characters.Persone;

public class Healing {
    public static void healing(MainPersone hero){
        if (hero.getMaxHp() > hero.getHp()) {
            hero.setHp(hero.getHp() + (int)(hero.getMaxHp()*0.2));
            if(hero.getMaxHp() < hero.getHp()){
                hero.setHp(hero.getMaxHp());
            }
            System.out.println("Ви полікувались ваше hp = " + hero.getHp());
        } else {
            System.out.println("Ви максимально здорові");
        }
    }
}
