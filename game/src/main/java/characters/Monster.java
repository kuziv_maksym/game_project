package characters;

public abstract class Monster extends Persone {
    public Monster(String name, int hp, int pow) {
        super(name, hp, pow);
    }

    public static Monster getRandomMonster() {
        double x = Math.random() * 3;
        if (x < 1) {
            return new Dog();
        } else {
            return new Rat();
        }
    }
}
