package characters;

public class Troll extends MainPersone {
    private final static int hp = 125;
    private final static int pow = 18;
    private final static int agi = 5;
    private final static int bra = 10;
    private final static int maxHp = 150;
    public Troll(String name) {
        super(name, hp, pow, agi, bra, maxHp);
    }
}
