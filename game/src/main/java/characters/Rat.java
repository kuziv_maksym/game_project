package characters;

public class Rat extends Monster {
    private static final String name = "Rat";
    private static final int hp = 50;
    private static final int pow = 13;
    public Rat() {
        super(name, hp, pow);
    }
}
