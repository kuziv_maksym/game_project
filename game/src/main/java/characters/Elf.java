package characters;


public class Elf extends MainPersone {
    private final static int hp = 100;
    private final static int pow = 17;
    private final static int agi = 10;
    private final static int bra = 15;
    private final static int maxHp = 100;
   public Elf(String name) {
       super(name, hp, pow, agi, bra, maxHp);
   }
}
