package characters;

public abstract class MainPersone extends Persone {
    int agi;
    int bra;
    int maxHp;
    public MainPersone(String name, int hp, int pow,
                       int agi, int bra, int maxHp) {
        super(name, hp, pow);
        this.agi = agi;
        this.bra = bra;
        this.maxHp = maxHp;
    }

    public void setMaxHp(int maxHp) {
        this.maxHp = maxHp;
    }

    public int getMaxHp() {
        return maxHp;
    }

    public int getAgi() {
        return agi;
    }

    public void setAgi(int agi) {
        this.agi = agi;
    }

    public int getBra() {
        return bra;
    }

    public void setBra(int bra) {
        this.bra = bra;
    }
}
