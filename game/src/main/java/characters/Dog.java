package characters;

public class Dog extends Monster {
    private static final String name = "Dog";
    private static final int hp = 50;
    private static final int pow = 10;

    public Dog() {
        super(name, hp, pow);
    }
}
