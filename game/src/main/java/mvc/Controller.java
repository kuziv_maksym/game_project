package mvc;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import characters.Elf;
import characters.MainPersone;
import characters.Monster;
import characters.Troll;

import static actions.Fight.fight;


public class Controller {
    private Scanner sc = new Scanner(System.in);
    private View view = new View();
    private static final Logger firstLogger = LogManager.getLogger(Controller.class);

    public void mainLoop() {
        firstLogger.trace("Choosing hero.");
        //choosing hero
        View.display("Введіть ваше імя: ");
        MainPersone hero = init();

        firstLogger.warn("After creating main hero's character");
        Monster monster;

        firstLogger.debug("It doesn't work properly!");
        firstLogger.fatal("Rewrite this sh*t please");

        firstLogger.error("Error test...");
        while(hero.getHp() > 0){
        monster = Monster.getRandomMonster();
            View.display("Вы сражаетесь с " + monster.getName());
            fight(monster, hero);
        }
    }



    public void chooseHeroClass() {
        System.out.println("Виберіть расу персонажа:");
        System.out.println("1. Ельф");
        System.out.println("2. Троль");
    }
//Initialize Type of hero
    private MainPersone init() {
        String name = sc.next();
        View.display("Виберіть расу персонажа:");
        View.display("1. Ельф");
        View.display("2. Троль");
        int pers = sc.nextInt();
        switch(pers) {
            case 1 : return new Elf(name);
            case 2 : return new Troll(name);
            default : {
                View.display("Choose only 1 or 2!");
                init();
                return null;
            }
        }
    }

    public Controller getController() {
        return new Controller();
    }
}
