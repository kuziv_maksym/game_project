package mvc;
import java.util.Scanner;


public class Application {
    private Controller controller;

    //starting main loop from controller
    public Application(Controller c){
        controller = c;
        c.mainLoop();
    }
    //two options of starting loop
    //In this case we will not need private field
    //Controller in Application class
    public static void run(Controller c) {
        c.mainLoop();
    }
    public static void main(String[] args) {
        Controller c = new Controller(); //Ото переробити інфо 100%

        c.mainLoop();
        System.out.println("GG");
    }
}
